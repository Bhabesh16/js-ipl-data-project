const csvParser = require("csv-parser"); // Package for convert file csv to json
const fs = require("fs"); // File System Module

function highestPlayerOfTheMatch() {

    const matchesFilePath = __dirname + "/../data/matches.csv"; // Path for matches.csv
    const resultsFilePath = __dirname + "/../public/output/6-player-won-highest-player-of-the-match-each-season.json"; // Path for the output file

    const matches = [];

    // Reading the matches.csv file using csvParser and storing it into matches

    fs.createReadStream(matchesFilePath)
        .pipe(csvParser())
        .on("data", (data) => matches.push(data))
        .on("end", () => {

            // storing every player won man of the match in each season

            const playersWonManOfMatchEachSeason = matches.reduce((playersWonEachSeason, currentMatch) => {

                const playerName = currentMatch.player_of_match;
                const currentSeason = currentMatch.season;

                if (playersWonEachSeason[currentSeason] === undefined) {
                    playersWonEachSeason[currentSeason] = [];
                }

                const playerCurrentSeason = playersWonEachSeason[currentSeason].find((player) => {
                    return player.name === playerName;
                });

                if (playerCurrentSeason === undefined) {
                    playersWonEachSeason[currentSeason].push({
                        name: playerName,
                        won: 1
                    });
                } else {
                    playerCurrentSeason.won += 1;
                }

                return playersWonEachSeason;
            }, {});

            // sorting and storing the player who won man of the match in each season

            const playerWonHighestAward = {};

            for (const [currentSeason, playersWonManOfMatch] of Object.entries(playersWonManOfMatchEachSeason)) {

                const newValue = playersWonManOfMatch.sort((player1, player2) => {
                    return player2.won - player1.won;
                });

                playerWonHighestAward[currentSeason] = newValue[0];
            }

            // converting the results to a string

            const jsonString = JSON.stringify(playerWonHighestAward);

            //writing the output into 6-player-won-highest-player-of-the-match-each-season.json file

            fs.writeFile(resultsFilePath, jsonString, (error) => {
                if (error) {
                    console.log("Error writing file", error);
                } else {
                    console.log("Successfully wrote file");
                }
            });
        });
}

highestPlayerOfTheMatch();